$(function () {
    $('[data-toggle="tooltip"]').tooltip();
    $('[data-toggle="popover"]').popover();
    $('.carousel').carousel({
      interval: 3000
    });
    $('#modal-recomendacion').on('show.bs.modal', function (e) {
      console.log('funcionando');
      $('#btnmodal').prop('disabled', true);
      $('#btnmodal').toggleClass('btn-primary color-modal-press');
    });

    $('#modal-recomendacion').on('hide.bs.modal', function(e){
      $('#btnmodal').prop('disabled', false);
      $('#btnmodal').toggleClass('btn-primary color-modal-press');
    });
});